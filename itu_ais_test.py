import requests
import json
from file_handling_library import *
from credentials_thh import config

mypath="input/fartoyreg_leppefisk-2018-08-21_1458.json"
outpath="input/fartoyreg_leppefisk_bw"
bwpath="https://www.barentswatch.no/api/v1/geodata/vessel/"
token ={}

def get_client_token():

    req = requests.post("https://www.barentswatch.no/api/token",
            data={
                  'grant_type': 'client_credentials',
                  'client_id': config['api_user'],
                  'client_secret': config['api_password']
            },
            params={},
            headers={'content-type': 'application/x-www-form-urlencoded'})

    if req.status_code == requests.codes.ok:
              print "status: "+ str(req.status_code)
              return req.json()
    else:
      print "Error:" 
      print "status: "+ str(req.status_code)
      return req.json()


def bwRequest(token, url):
      #url = "https://www.barentswatch.no/api/v1/geodata/download/fishingfacility/?format=JSON"
      token2 = token['access_token'].encode("ascii", "ignore")

      headers ={
            'authorization': "Bearer " + token2,
        'content-type': "application/json",
      }

      response = requests.get(url, headers=headers)
      if response.status_code == requests.codes.ok:
            return response.json()
      else:
            return "Error"


def parseShipList(token,shiplist):
      for ship in shiplist:
            if ship['itu_mmsi'] is not None:
                  data = bwRequest(token,bwpath+ship['itu_mmsi'])
                  if data is not None and 'message' not in data:
                        if 'ais' in data:
                              ship['bwAIS']=data['ais']
                        else:
                              ship['bwAIS']='NILL'
                  else:
                        ship['bwAIS']='NILL'
      return shiplist


def loadData(mypath):
      print mypath
      with open(mypath) as f:
            print f
            data = json.load(f)
            return data

#print config
ships=loadData(mypath)
#print ships
token= get_client_token()
#print token
ships = parseShipList(token,ships)
#print ships[1]['itu_mmsi']
#test = bwRequest(token,bwpath+ships[1]['itu_mmsi'])
#print test
listDict2jsonFile(ships,outpath)
