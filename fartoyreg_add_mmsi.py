#!/usr/bin/python
from itu_mars_library import *
from file_handling_library import *
import sys, getopt

timestamp= 	time.strftime("%Y-%m-%d_%H%M") # timestamp used for outputfile
#inputFile="shiplist_test.json" #jsonFile with a list of ships to fetch format=listDict
geo=[]
adm=[]
#input_file="input/fartaoyreg_2018-07-03.json"
input_file="input/test.json"
outputfile="output/fartoyreg_"
ituFile="output/itu_data_"
itu_data=[]
outputData=[]

#def parse_file(input_file):


def download_both_areas():
	geo = get_geo_areas()
	listDict2jsonFile(geo,'geo_areas')
	adm = get_adm_areas()
	listDict2jsonFile(adm,'adm_areas')

def parse_ship_file(file):
	with open(file, mode='r') as jsonfile:
		data = json.load(jsonfile)
		for ship in data:
			if "baat_radio" in ship:
				if ship["baat_radio"] is not None:
					itu_ship=itu_search_ship_by_IRCS(ship['baat_radio'])
					if itu_ship is not None:
						itu_data.append(itu_ship)
						ship['itu_mmsi']=itu_ship['mmsi']
						ship['itu_name']=itu_ship['shipname']
					else:
						ship['itu_mmsi']="NotFound"
					time.sleep(timeDelay)

		return data



outputData = parse_ship_file(input_file)
listDict2jsonFile(outputData,outputfile)
listDict2jsonFile(itu_data,ituFile)

'''def main(argv):
   ircs = ''
  '''