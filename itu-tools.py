#!/usr/bin/python
from itu_mars_library import *
from file_handling_library import *
import sys, getopt

inputFile="shiplist_test.json" #jsonFile with a list of ships to fetch format=listDict
geo=[]
adm=[]
ircs="LD"

def download_both_areas():
	geo = get_geo_areas()
	listDict2jsonFile(geo,'geo_areas')
	adm = get_adm_areas()
	listDict2jsonFile(adm,'adm_areas')

def parse_ship_file(file):
	with open(file, mode='r') as jsonfile:
		data = json.load(jsonfile)
		ships = itu_ship_dict(data)
		listDict2jsonFile(ships,"ituresult")

def get_ship_data(callsign):
	shipdata=itu_search_ship_by_IRCS(callsign)
	print shipdata
	return shipdata

download_both_areas()
ship=get_ship_data(ircs)
listDict2jsonFile(ship,ircs)

'''def main(argv):
   ircs = ''
  '''