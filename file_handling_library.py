import json
import time
import io

timestamp= 	time.strftime("%Y-%m-%d_%H%M") # timestamp used for outputfile

def dump2file(data,filename):
	with open(filename, mode='wt') as file:
		file.write(str(data))

def listDict2jsonFile(listDict,filename):
	filename=filename+"-"+timestamp+".json"
	with io.open(filename, mode='w',encoding='utf-8') as outfile:
		#json.dump(listDict, outfile)
		outfile.write(unicode(json.dumps(listDict, ensure_ascii=False)))
	print "wrote data to: "+filename