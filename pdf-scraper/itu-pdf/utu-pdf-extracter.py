import Tkinter
import camelot
from pandas import DataFrame

## Using camelot library for PDF parsing
##Documentation:https://camelot-py.readthedocs.io/en/master/

firstpage=3
lastpage=4741
#print tables[0].df
#frames[]
def readPDF(file):
	tables = camelot.read_pdf(file,flavor='stream',pages='1-4',strip_text=' .\n')
	#print tables
	#print tables[0]

	return tables

def mergeTables(tables):
	df1=DataFrame()
	#df1.append(tables[0])
	print type(tables)
	for table in tables:
		table.df.drop(table.df.index[0])
		table.df.drop(table.df.index[0])
		df1 = df1.append(table.df)

	df1.columns=['mmsi','IRCS','selectic callnumber','Station name','AdmArea','Lifeboats','EPIRBs','GClassification', 'IClassification','Regnr','GTonnage','person cap','TelegrapF','TelphoneF','AAIC','info','ext']

	return df1


def pandaframe2json(df):
	df.to_json('temp22.json', orient='records', lines=True)

tables=readPDF('itu_mars_page_3_1000.pdf')
table=mergeTables(tables)
pandaframe2json(table)

#tables.export('test2.json', f='json', compress=False)