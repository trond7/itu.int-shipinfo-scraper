import requests
from lxml import html
from bs4 import BeautifulSoup
import json
import time
import io

#Config variables
headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}
url = "https://www.itu.int/mmsapp/ShipStation/list"	
#callsign="LIOD"
timestamp= 	time.strftime("%Y-%m-%d_%H%M") # timestamp used for outputfile
inputFile="shiplist_test.json" #jsonFile with a list of ships to fetch format=listDict
timeDelay=5 

def itu_search_ship_by_IRCS(ircs):
	#Initial start to get a session and mandatory variables for the post request
	s = requests.Session()
	#r = s.get(url)
	#soup = BeautifulSoup(r.content)
	# finding the breadcrump token to pass in the seach post request
	#Breadcrumb = soup.find('input', {'id': 'Breadcrumb'}).get('value')
	# Setting all mandatory post variables
	payload = {'Search.CallSign': ircs, 'viewCommand':'Search'}

	#Do the search request (post) with the ircs identifier
	print "fetching: "+ircs+" from itu.int"
	r = s.post(url, headers=headers, data=payload)
	soup = BeautifulSoup(r.content,"html.parser")
	table = soup.find("table", attrs={"class":"table"})
	last_row = table("tr")[-1]
	
	# The first tr/th contains the field names.
	#headings = [th.get_text() for th in table.find("tr").find_all("th")]
	#print headings
	dataset = {}
	mycnt=0
	varNames=['shipname', 'ircs','mmsi','itu_adm','itu_area','imo','EPIRB','update_date']
	for cell in last_row.find_all('td')[1:]:
		varName=varNames[mycnt]
		mycnt+=1
		dataset[varName]=cell.text
	dataset['itu_nr'] = soup.find('button', {'name': 'onview'}).get('value')
	
	return dataset
	
	
def fetch_ship_data(itu_id):
	# To parse ship detail page to get all atributes
	# Not in use yet, Not finish yet

 	url= "https://www.itu.int/mmsapp/shipstation/one/"+itu_id
 	#payload = {'Search_CallSign': ircs}
 	r = requests.get(url)
 	tree = html.fromstring(r.content)
	ship={}
	ship['itu_nr']=itu_id
 	ship['name'] = tree.xpath('//div[div="Ship Name"]/label[@class="form-control"]/text()')
 	ship['ircs'] = tree.xpath('//div[div="Call Sign"]/label[@class="form-control"]/text()')
 	ship['mmsi'] = tree.xpath('//div[div="MMSI"]/label[@class="form-control"]/text()')
 	ship['imo'] = tree.xpath('//div[div="Vessel Identification Number"]/label[@class="form-control"]/text()')

 	return ship


def itu_ship_dict(ship_listDict):
	#This takes a list of dict where the dict has ircs as a atribute
	ships=[]
	for element in ship_listDict:
		ship={}
		ship = itu_search_ship_by_IRCS(element['ircs'])
		ships.append(ship)
		time.sleep(3)
	return ships
	
def parse_ship_file(file):
	with open(file, mode='r') as jsonfile:
		data = json.load(jsonfile)
		ships = itu_ship_dict(data)
		listDict2jsonFile(ships,"ituresult")

def dump2file(data,filename):
	with open(filename, mode='wt') as file:
		file.write(str(data))

def listDict2jsonFile(listDict,filename):
	filename=filename+"-"+timestamp+".json"
	with io.open(filename, mode='w',encoding='utf-8') as outfile:
		#json.dump(listDict, outfile)
		outfile.write(unicode(json.dumps(listDict, ensure_ascii=False)))

def get_admin_areas():
	s = requests.Session()
	r = s.get(url)
	soup = BeautifulSoup(r.content)
	admin_areas=[]
	adm = soup.find('select', {'name'='Search.GeographicalArea.SelectedId'})
	for option in adm.find_all('option')[1:]:
		temp={}
		temp['id']=option['value']
		temp['name']=option.text
		admin_areas.append(temp)
	print admin_areas	





get_admin_areas()
#Fetch ships from file(jsonfile with listDict)
#parse_ship_file(myfile)
#fetch a single ship based on ircs
#shipinfo = itu_search_ship_by_IRCS(callsign)



#Links to API endpoints
'''
Administration: 121 = Norway
Geographical Area: 166 = Norway
Get all Geographical areas
https://www.itu.int/mmsapp/shipstation/GetGeographicalAreasByAdministration
https://www.itu.int/mmsapp/shipstation/GetGeographicalAreasByAdministration?id=121
https://www.itu.int/mmsapp/shipstation/GetAdministrationIdByGeographicalAreaId?id=168
'''