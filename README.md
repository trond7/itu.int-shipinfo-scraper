# itu.int shipinfo scraper

## Description
* This simple script scrape the itu.int webpage (MARS database) for shipinfo.
* It is a simple html scraper written in python.
* It can scrape a single ship or bulk fetch a list of ships
* Please do not use this to hammer itu.int to much. (it has a time dely variable that can be tuned for this)

## Background
* itu.int has a database with ship information called [MARS](https://www.itu.int/en/ITU-R/terrestrial/mars/Pages/default.aspx)
* The database contains mainly telecomunication informatin for the ships
* The database contains telecomunication identifiers for the ships

The script uses the IRCS / callsign identifier to search/fetch a ship and then collect the following atributes:

attribute|itu name|description|readmore link
--|--|--|--
shipname|Ship Name| the name of the ship| . 
ircs|Call Sign| radio/VHF/AIS unique identifiers to ships | https://en.wikipedia.org/wiki/Maritime_call_sign
imo|Vessel Identification Number| Uniqe identifiers for large ships | https://en.wikipedia.org/wiki/IMO_number
mmsi|MMSI|Maritime Mobile Service Identity,nine digit number. |https://en.wikipedia.org/wiki/Maritime_Mobile_Service_Identity
itu_area|Geographical Area| |https://www.itu.int/en/ITU-R/terrestrial/fmd/Pages/geo_area_list.aspx
itu_adm|Administration| |
EPIRB|EPIRB| emergency position-indicating radio beacon |https://en.wikipedia.org/wiki/Emergency_position-indicating_radiobeacon_station
update_date|Update date| |



## Warning
* HTML scraping is a very fragile way to parse data.
* If/when itu.int changes their webside this script can brake totaly.
* Not only can it stop working but posible it can actualy work but get the key/values wrong. Test before use.
* The script was implemented 2018.01.31 and was working for my own needs at that time.

## License / warranty
* The code is provided "as is" and has no warranty whatsoever.
* This script is free software: you are free to change and redistribute it.
